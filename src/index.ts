import 'module-alias/register';
import path from 'path';
import Day9 from './2021/09';
import { solvePart1, solvePart2 } from './day';

// ? add "jest.config.js"
// ? add any non source js files

import fs from 'fs';
import util from 'util';
import Day10 from './2021/10';
import Day11 from './2021/11';
const logFile = fs.createWriteStream(path.join(__dirname, 'debug.log'), { flags: 'w' });
const logStdout = process.stdout;

console.log = function (data: any) { //
  logFile.write(util.format(data) + '\n');
  logStdout.write(util.format(data) + '\n');
};

const day = Day11;

const args = process.argv.slice(2);

if (args[0] !== undefined) {
  console.log(solvePart1(day, './input/2021/examples'));
  console.log(solvePart2(day, './input/2021/examples'));
} else {
  console.log(solvePart1(day, './input/2021'));
  console.log(solvePart2(day, './input/2021'));
}
