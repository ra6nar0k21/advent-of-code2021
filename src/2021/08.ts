/* eslint-disable @typescript-eslint/no-non-null-assertion */
import Day from '@/day';

/**
 *     AAA
 *    B   C
 *    B   C
 *     DDD
 *    E   F
 *    E   F
 *     ggg
 */

const Day8: Day = {
  dayNumber: 8,
  solvePart1(filedata: string): number {
    const data = filedata.split('\n');
    const uniqueSignalPatterns: Array<string> = [];
    const digitOuputValues: Array<string> = [];

    data.forEach(val => {
      uniqueSignalPatterns.push(val.split(' | ')[0]);
      digitOuputValues.push(val.split(' | ')[1]);
    });

    const signalPatterns = [
      6,
      2,
      5,
      5,
      4,
      5,
      6,
      3,
      7,
      6,
    ];

    const result: Array<number> = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    for (let i = 0; i < digitOuputValues.length; i++) {
      for (const signal of digitOuputValues[i].split(' ')) {
        for (let j = 0; j < signalPatterns.length; j++) {
          if (signal.length === signalPatterns[j]) {
            result[j]++;
            break;
          }
        }
      }
    }

    return result.filter((val, index) => index === 1 || index === 4 || index === 7 || index === 8).reduce((prev, cur) => (prev + cur));
  },
  solvePart2(filedata: string): number {
    const data = filedata.split('\n');
    const uniqueSignalPatterns: Array<string> = [];
    const digitOuputValues: Array<string> = [];

    data.forEach(val => {
      uniqueSignalPatterns.push(val.split(' | ')[0]);
      digitOuputValues.push(val.split(' | ')[1]);
    });

    // Decodes a line
    const decode = (pattern: string, outputValue: string): string => {
      const patterns: string[][] = pattern.split(' ').map(p => p.split(''));

      const numberOne = patterns.find(p => p.length === 2)!;
      const numberFour = patterns.find(p => p.length === 4)!;
      const numberSeven = patterns.find(p => p.length === 3)!;
      const numberEight = patterns.find(p => p.length === 7)!;

      const sidesCDE = patterns
        .filter(p => p.length === 6)
        .flatMap(p => numberEight.filter(pp => !p.includes(pp)));

      const sideE = sidesCDE.find(p => !numberFour.includes(p));

      const numberNine = numberEight.filter(p => p !== sideE);

      const sideC = sidesCDE.find(p => numberOne.includes(p));

      const numberSix = numberEight.filter(p => p !== sideC);

      const sideD = sidesCDE.find(p => p !== sideC && p !== sideE);
      const sideF = numberOne.find(p => p !== sideC)!;

      const numberZero = numberEight.filter(p => p !== sideD);

      const sideB = numberFour.find(p => ![sideC, sideD, sideF].includes(p));
      const numberTwo = numberEight.filter(p => p !== sideB && p !== sideF);
      const numberFive = numberEight.filter(p => p !== sideC && p !== sideE);
      const numberThree = numberTwo.filter(p => p !== sideE);
      numberThree.push(sideF);

      // console.log({ One: numberOne.sort((a, b) => (a.charCodeAt(0) - b.charCodeAt(0))).join('') });
      // console.log({ Two: numberTwo.sort((a, b) => (a.charCodeAt(0) - b.charCodeAt(0))).join('') });
      // console.log({ Three: numberThree.sort((a, b) => (a.charCodeAt(0) - b.charCodeAt(0))).join('') });
      // console.log({ Four: numberFour.sort((a, b) => (a.charCodeAt(0) - b.charCodeAt(0))).join('') });
      // console.log({ Five: numberFive.sort((a, b) => (a.charCodeAt(0) - b.charCodeAt(0))).join('') });
      // console.log({ Six: numberSix.sort((a, b) => (a.charCodeAt(0) - b.charCodeAt(0))).join('') });
      // console.log({ Seven: numberSeven.sort((a, b) => (a.charCodeAt(0) - b.charCodeAt(0))).join('') });
      // console.log({ Eight: numberEight.sort((a, b) => (a.charCodeAt(0) - b.charCodeAt(0))).join('') });
      // console.log({ Nine: numberNine.sort((a, b) => (a.charCodeAt(0) - b.charCodeAt(0))).join('') });
      // console.log({ Zero: numberZero.sort((a, b) => (a.charCodeAt(0) - b.charCodeAt(0))).join('') });

      const patternMap: Map<string[], string> = new Map<string[], string>([
        [numberZero, '0'],
        [numberOne, '1'],
        [numberTwo, '2'],
        [numberThree, '3'],
        [numberFour, '4'],
        [numberFive, '5'],
        [numberSix, '6'],
        [numberSeven, '7'],
        [numberEight, '8'],
        [numberNine, '9'],
      ]);

      // console.log({ numberZero, numberOne, numberTwo, numberThree, numberFour, numberFive, numberSix, numberSeven, numberEight, numberNine });
      // console.log({ sideA, sideB, sideC, sideD, sideE, sideF });

      let result = '';

      for (const elem of outputValue.split(' ')) {
        for (const [k, v] of patternMap.entries()) {
          const kSort = k.sort((a, b) => (a.charCodeAt(0) - b.charCodeAt(0))).join('');
          const elemSort = elem.split('').sort((a, b) => (a.charCodeAt(0) - b.charCodeAt(0))).join('');
          // console.log(kSort === elemSort, v);
          if (kSort === elemSort) {
            result += v;
            break;
          }
        }
      }
      return result;
    };

    let result = 0;

    for (let i = 0; i < uniqueSignalPatterns.length; i++) {
      result += Number(decode(uniqueSignalPatterns[i], digitOuputValues[i]));
    }

    return result;
  },
};

export default Day8;
