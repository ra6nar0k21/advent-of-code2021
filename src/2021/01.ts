import Day from '../day';

const Day1: Day = {
  dayNumber: 1,
  solvePart1(filedata: string): number {
    const content = filedata.split('\n');

    let prev = 99999999999;
    let result = 0;

    for (const line of content) {
      if (Number(line.trim()) > prev) {
        result++;
      }
      prev = Number(line.trim());
    }

    return result;
  },
  solvePart2(filedata: string): number {
    const content = filedata.split('\n');

    let prev = 9999999999;
    let result = 0;

    for (let i = 1; i < content.length - 1; i++) {
      const a = Number(content[i - 1]);
      const b = Number(content[i]);
      const c = Number(content[i + 1]);

      const sum = a + b + c;
      if (sum > prev) {
        result++;
      }

      prev = sum;
    }

    return result;
  },
};

export default Day1;
