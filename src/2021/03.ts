import Day from '@/day';
import * as fs from 'fs';

const Day3: Day = {
  dayNumber: 3,
  solvePart1(filecontent: string): number {
    const content = filecontent.split('\n');

    let mostCommon = '0b';
    let leastCommon = '0b';

    for (let i = 0; i < content[0].length; i++) {
      let zeroCount = 0;
      let oneCount = 0;

      for (let j = 0; j < content.length; j++) {
        if (content[j].charAt(i) === '0') {
          zeroCount++;
        } else {
          oneCount++;
        }
      }

      if (zeroCount > oneCount) {
        mostCommon += '0';
        leastCommon += '1';
      } else {
        mostCommon += '1';
        leastCommon += '0';
      }
    }

    return Number(mostCommon) * Number(leastCommon);
  },
  solvePart2(filecontent: string): number {
    const content = filecontent.split('\n');

    const solve = (arr: Array<string>, bit: number, cb: (ones: Array<string>, zeros: Array<string>) => Array<string>): Array<string> => {
      if (arr.length === 1) {
        return arr;
      }

      const zeros = [];
      const ones = [];

      for (const line of arr) {
        if (line.charAt(bit) === '0') {
          zeros.push(line);
        } else {
          ones.push(line);
        }
      }

      return cb(ones, zeros);
    };

    let oxygen = Array.from(content);
    let o2 = Array.from(content);

    for (let i = 0; i < content[0].length; i++) {
      oxygen = solve(oxygen, i, (o, z) => {
        if (o.length >= z.length) {
          return o;
        }
        return z;
      });
      o2 = solve(o2, i, (o, z) => {
        if (o.length < z.length) {
          return o;
        }
        return z;
      });
    }

    return Number(`0b${oxygen[0]}`) * Number(`0b${o2[0]}`);
  },
};
export default Day3;
