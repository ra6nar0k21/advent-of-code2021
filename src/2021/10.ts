import Day from '@/day';

const corruptedPointMap = new Map<string, number>([
  [')', 3],
  [']', 57],
  ['}', 1197],
  ['>', 25137],
]);

const isOpening = (bracket: string) => {
  return bracket === '(' || bracket === '[' || bracket === '<' || bracket === '{';
};

const isCorespondingClosing = (bracket: string, other: string) => {
  return (bracket === ')' && other === '(') ||
    (bracket === '}' && other === '{') ||
    (bracket === ']' && other === '[') ||
    (bracket === '>' && other === '<');
};

const checkCorrupted = (brackets: Array<string>) => {
  const bracketQueue: Array<string> = [];
  for (const bracket of brackets) {
    if (isOpening(bracket)) {
      bracketQueue.push(bracket);
    } else {
      if (isCorespondingClosing(bracket, bracketQueue[bracketQueue.length - 1])) {
        bracketQueue.pop();
      } else {
        return corruptedPointMap.get(bracket) ?? 0;
      }
    }
  }
  return 0;
};

const getBracketCompletionVal = (bracket: string) => {
  if (bracket === '(') {
    return 1;
  }
  if (bracket === '[') {
    return 2;
  }
  if (bracket === '{') {
    return 3;
  }
  return 4;
};

const autoComplete = (brackets: Array<string>) => {
  const bracketQueue: Array<string> = [];
  for (const bracket of brackets) {
    if (isOpening(bracket)) {
      bracketQueue.push(bracket);
    } else {
      bracketQueue.pop();
    }
  }
  let result = 0;
  while (bracketQueue.length > 0) {
    const bracket = bracketQueue.pop();
    if (bracket === undefined) {
      break;
    }

    result *= 5;
    result += getBracketCompletionVal(bracket);
  }
  return result;
};

const Day10: Day = {
  dayNumber: 10,
  solvePart1(filedata: string): number {
    const bracketsData = filedata.split('\n');
    let result = 0;
    bracketsData.forEach((brackets) => {
      result += checkCorrupted(brackets.split(''));
    });
    return result;
  },
  solvePart2(filedata: string): number {
    const bracketsData = filedata.split('\n');
    const results: Array<number> = [];
    bracketsData.forEach((brackets) => {
      if (checkCorrupted(brackets.split('')) === 0) {
        results.push(autoComplete(brackets.split('')));
      }
    });
    results.sort((a, b) => (b - a));
    const index = (results.length - 1) / 2;
    return results[index];
  },
};

export default Day10;
