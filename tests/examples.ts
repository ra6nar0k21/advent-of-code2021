import { describe, expect, it } from '@jest/globals';
import Day1 from '../src/2021/01';
import Day2 from '../src/2021/02';
import Day3 from '../src/2021/03';
import Day4 from '../src/2021/04';
import Day5 from '../src/2021/05';
import Day6 from '../src/2021/06';
import Day7 from '../src/2021/07';
import Day8 from '../src/2021/08';
import Day9 from '../src/2021/09';
import Day10 from '../src/2021/10';
import Day11 from '../src/2021/11';
import { solvePart1, solvePart2 } from '../src/day';

describe('Day 1 examples', () => {
  it('should return result of part 1', () => {
    expect(solvePart1(Day1, './input/2021/examples')).toBe(7);
  });

  it('should return result of part 2', () => {
    expect(solvePart2(Day1, './input/2021/examples')).toBe(5);
  });
});
describe('Day 2 examples', () => {
  it('should return result of part 1', () => {
    expect(solvePart1(Day2, './input/2021/examples')).toBe(150);
  });

  it('should return result of part 2', () => {
    expect(solvePart2(Day2, './input/2021/examples')).toBe(900);
  });
});
describe('Day 3 examples', () => {
  it('should return result of part 1', () => {
    expect(solvePart1(Day3, './input/2021/examples')).toBe(198);
  });

  it('should return result of part 2', () => {
    expect(solvePart2(Day3, './input/2021/examples')).toBe(230);
  });
});
describe('Day 4 examples', () => {
  it('should return result of part 1', () => {
    expect(solvePart1(Day4, './input/2021/examples')).toBe(4512);
  });

  it('should return result of part 2', () => {
    expect(solvePart2(Day4, './input/2021/examples')).toBe(1924);
  });
});
describe('Day 5 examples', () => {
  it('should return result of part 1', () => {
    expect(solvePart1(Day5, './input/2021/examples')).toBe(5);
  });

  it('should return result of part 2', () => {
    expect(solvePart2(Day5, './input/2021/examples')).toBe(12);
  });
});
describe('Day 6 examples', () => {
  it('should return result of part 1', () => {
    expect(solvePart1(Day6, './input/2021/examples')).toBe(5934);
  });

  it('should return result of part 2', () => {
    expect(solvePart2(Day6, './input/2021/examples')).toBe(26984457539);
  });
});
describe('Day 7 examples', () => {
  it('should return result of part 1', () => {
    expect(solvePart1(Day7, './input/2021/examples')).toBe(37);
  });

  it('should return result of part 2', () => {
    expect(solvePart2(Day7, './input/2021/examples')).toBe(168);
  });
});
describe('Day 8 examples', () => {
  it('should return result of part 1', () => {
    expect(solvePart1(Day8, './input/2021/examples')).toBe(26);
  });

  it('should return result of part 2', () => {
    expect(solvePart2(Day8, './input/2021/examples')).toBe(61229);
  });
});
describe('Day 9 examples', () => {
  it('should return result of part 1', () => {
    expect(solvePart1(Day9, './input/2021/examples')).toBe(15);
  });

  it('should return result of part 2', () => {
    expect(solvePart2(Day9, './input/2021/examples')).toBe(1134);
  });
});

describe('Day 10 examples', () => {
  it('should return result of part 1', () => {
    expect(solvePart1(Day10, './input/2021/examples')).toBe(26397);
  });

  it('should return result of part 2', () => {
    expect(solvePart2(Day10, './input/2021/examples')).toBe(288957);
  });
});

describe('Day 11 examples', () => {
  it('should return result of part 1', () => {
    expect(solvePart1(Day11, './input/2021/examples')).toBe(1656);
  });

  it('should return result of part 2', () => {
    expect(solvePart2(Day11, './input/2021/examples')).toBe(195);
  });
});
